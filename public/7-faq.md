# FAQ: Installing OpenHPC.

Frequently Asked Questions for installing OpenHPC 1.3.x on a Virtual Cluster.

## Vagrant

+ ISSUE: The machine with the name "client01" was not found configured for this Vagrant environment? +

    This happens if the vagrantfile is modified during a live vagrant deployment. The original vagrant image will not be consistent with the vagrantfile any more.

    Run the following command:

    ```bash
    vagrant global-status --prune
    ```

    Once done, remove the Virtualbox VM from the Virtualbox Manager.

+ QUESTION: How do I find the vagrant image ID number? +

    The `vagrant global-status` command will show you your image ID.

+ QUESTION: How do I save the current VM state and suspend the VM? +

    Use the `vagrant suspend` command from your terminal.

+ QUESTION: How do I resume the current VM from a suspended / saved state? +

    Use the `vagrant resume` command from your terminal.

+ QUESTION: My Vagrant VM loses the host-shared folder, how do I fix this? +
    
    Reload the vagrant configuration using the `vagrant reload` command.

    > [!TIP]
    > This can occur when rebooting a VM from within the guest OS - instead, reboot the VM using vagrant commands.

+ QUESTION: How do I make a Vagrant VM snapshot? +

    To save a snapshot:
    `vagrant snapshot save <name>`

    Alternatively (but not interchangeably)
    `vagrant snapshot push`


## xCAT

+ ISSUE: "Ignoring line compute00 in /etc/hosts, address seems malformed." when running command "makedns -n" +

    Make sure that the `/etc/hosts` file is configured and aligned properly

    ```text
    127.0.0.1       sms-host        sms-host

    127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4

    ::1         localhost localhost.localdomain localhost6 localhost6.localdomain6

    10.10.10.10 sms-host    sms-host.hpcnet

    10.10.10.100 compute00  compute00.hpcnet

    10.10.10.101 compute01  compute01.hpcnet
    ```

+ QUESTION: How do I edit xCAT networks? +

    The `tabedit networks` command will open a text editor for you to correct/adjust the xCAT networks table.

+ QUESTION: How do I list all of my nodes? +

    The `lsdef -t node -l` command will lists all defined compute nodes.

+ QUESTION: How to delete a node definition from xCAT? +

    The `rmdef -t node compute01` command removes the specified node (`compute01` in this example).

+ QUESTION: How to delete all node definitions? +
    
    The `rmdef -t node -o compute` command removes defined compute nodes.

+ ISSUE: Trouble resolving hostnames in xCAT +

    `syntax issue with definitions`

    A very subtle bug can occur with the translation of `<CR><LF>` characters from the host OS to the virutal OS (e.g. Windows 10 to CentOS) - this can cause the environment variables defined in `setenv.c` to not translate properly.

    To confirm this is not the issue, run the following command and note the output:
    `lsdef -t node -l`

    ```text
    Object name: compute01

        arch=x86_64
        currstate=netboot centos7.6-x86_64-compute
        groups=compute,all
        ip=10.10.10.101
        mac=08:00:27:F5:9A:31
        mgt=ipmi
        netboot=xnba
        os=centos7.6
        postbootscripts=otherpkgs
        postscripts=syslog,remoteshell,syncfiles
        profile=compute
        provmethod=centos7.6-x86_64-netboot-compute
        serialport=0
        serialspeed=115200
    ```

    There should not be a gap in the line between `Object name` and the `arch=` line … if there is, you have a `CRLF` problem.

    Redefine the node without the `CRLF` corruption - that involves deleting the existing node definition and recreating it with `mkdef`.


## Slurm

+ ISSUE: Time drift on compute nodes +

    You may an issue such as the one here:

    ```text
    error: Munge decode failed: Expired credential
    [2020-04-20T21:09:28.739] ENCODED: Mon Apr 20 18:10:18 2020
    [2020-04-20T21:09:28.739] DECODED: Mon Apr 20 21:09:28 2020
    [2020-04-20T21:09:28.739] error: authentication: Expired credential
    [2020-04-20T21:09:28.739] error: slurm_receive_msgs: Protocol authentication
    ```

    To solve this, run the following commands:

    ```bash
    systemctl stop ntpd
    ntpd -gq
    systemctl start ntpd
    ```

+ ISSUE: Jobs are stuck in CG state +

    This may occur as a problem with simply copying `/etc/hosts`.

    Please refer to [this OpenHPC post](https://groups.io/g/OpenHPC-users/message/3580?p=,,,20,0,0,0).

    Try to cycle the nodes with Slurm:

    ```bash
    scontrol update NodeName=compute[00-01] State=down Reason=hung_proc
    scontrol update NodeName=compute[00-01] State=resume
    ```


+ QUESTION: How do I check that a node is reachable? +

    Running the command `scontrol ping` will produce:

    ```text
    Slurmctld(primary) at sms-host is UP
    ```

+ QUESTION: How do I set a node to IDLE? +

    The following command changes the state of a node to IDLE:
    ```bash
    scontrol update nodename=compute00 state=IDLE
    ```
    Make sure to change the `nodename` to the appropriate node.

## VirtualBox

+ ISSUE: vboxmanage command not found +

    Make sure to locate the path for this executable.

    In GitBash (Windows), for example:

    ```powershell
    /c/Program\ Files/Oracle/VirtualBox/VBoxManage.exe
    ```

    Or update the path:
    ```powershell
    export PATH=$PATH:/"C/Program Files/Oracle/VirtualBox/"
    ```

## PXE

+ ISSUE: "random: crng init done" +
    
    If the VM boot seems to pause at this point, consider upgrading the RAM on the VM for better performance (we recommend at least 3GB of RAM).

+ ISSUE: "work still pending +
    
    If the VM boot seems to pause at this point, consider upgrading the CPU on the VM for better performance (you need at least a 2 core VM for the workshop's default Slurm configuration).

+ ISSUE: Unable to boot from PXE. +

    Verify that the `sms-host` second network adapter is set as `'Internal network - hpcnet'`.

