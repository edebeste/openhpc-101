# OpenHPC 101

**Welcome to the HPC Ecosystems OpenHPC 1.3.x Training Guide!** We are so thrilled to have you along for the journey.

In the sections to follow you will find a comprehensive HOW-TO guide which can be used to set up OpenHPC&nbsp;1.3.x on a virtual cluster. This guide was developed for HPC Ecosystems Community as training material for our sites, to ensure parity across their HPC software environments. 

That being said, you **DO NOT** have to be a part of the HPC Ecosystems Community to use this guide. Anybody wishing to learn more about the depolyment of the OpenHPC&nbsp;1.3.x software stack is likely to find value in its contents. Let's jump right in!
