# 2. SMS Host Setup

## 2.0. Chapter Overview

This section will guide you through the setup of your **System Management Server (SMS)** host - the node that is responsible for managing the Virtual Cluster. This is often also called the **management node, master node, boss node, or host node.**

In the previous chapter we downloaded Virtualbox, Vagrant and a Vagrantfile to prepare the Virtual Cluster framework but no VMs were deployed.

In this chapter, the SMS host will be deployed (using a Vagrant definition to ensure system parity) and have the basic OpenHPC management framework installed. Ultimately the Virtual Cluster will consist of **three VM’s**, one SMS host and two clients (refer to [Figure 1](1-getting_ready#fig1)). 

> [!NOTICE]
> Anticipated time to complete this chapter: 80 to 90 minutes.

## 2.1. Deploy the SMS host VM

> [!VIDEO]
> Start of video [2.01 - Deploy the SMS host VM](https://www.youtube.com/watch?v=XBauaAC9exc&t=1s)

The command `vagrant up` will initialise the vagrant environment (and download the vagrant VM) - the entire process may take significant time, depending on your Internet connection.

> [!IMPORTANT]
> Be careful where you run `vagrant up`.
>
> Be sure to run `vagrant up` from the root of your VM directory - vagrant will install the VM at the current location as the root directory
> 
> For example:
> ```bash
> [~/openhpc-handson/student/]$ vagrant up
> ```

> [!TIP]
> Running an `ls` command (or equivalent) should list at least the `Vagrantfile` in the current working directory. The `vagrant up` instruction will reference this configuration file. If it is not located in the current working directory, Vagrant will climb back up the directory tree (towards the root) looking for the first `Vagrantfile` it can find.
>
> This could lead to the wrong Vagrantfile being used, so please ensure the correct path before running `vagrant up`.
>
> See [this link](https://www.vagrantup.com/docs/vagrantfile/) for more information.

1) Navigate to `student/` directory
2) Run the command:
    ```bash
    [~/openhpc-handson/student/]$ vagrant up
    ```

    The above commend will have vagrant read the parameters in the `Vagrantfile`, create the **Virtualbox** (or other) definition (such as vCPUs, RAM, NIC’s, etc.) and download and install the **CentOS** image onto the VM.

    > [!NOTICE]
    > This is currently **CentOS 7.7 64bit & is ~400MB in size**.
    >
    > Running the command `vagrant up` may fail with an error relating to *The IP address configured for the host-only network is not within the allowed ranges*. This is a known issue with the latest versions of VirtualBox – to resolve this problem, please follow: [Chapter 6. Virtual Networking (virtualbox.org)](https://www.virtualbox.org/manual/ch06.html#network_hostonly)

3) Once the VM is booted up for the first time, a Vagrant shell script has been defined to run yum install commands, all “behind-the-scenes”.

    > [!TIP]
    > You can SSH to the VM with any of the following methods:
    > - Using Vagrant:
    >    ```bash
    >    [~/openhpc-handson/student/]$ vagrant ssh
    >    ```
    > - Using any SSH client to 127.0.0.1:2229
    >
    > The default Vagrant username::password is `vagrant::vagrant`.
    >
    > While `vagrant ssh` is easiest, you may not have clipboard copy/paste available on the VM.

    > [!NOTICE]
    > Your host machine has a shared directory with the VM as defined in `Vagrantfile`. This shared directory is located on your host machine wherever `Vagrantfile` is located, and is located on the client VM in `/vagrant/`

> [!CONGRATS]
> You have deployed the **SMS (System Management Server) Host** Virtual Machine!

## 2.2. Prepare SMS Host parameters

> [!VIDEO]
> Start of video [2.02A - Prepare SMS Host Parameters](https://www.youtube.com/watch?v=XBauaAC9exc&list=PL2s6Yr_Iu_ke16_di1C3dowXHF-hRLmC9&index=4&t=366s)

Prior to continuing with the installation of the OpenHPC components, several commands must be issued into the Shell as per the OpenHPC recipe guide. To ensure all defined variables are set for the current Shell environment, one of the defined configuration files must be _'sourced'_.

> [!TIP]
> **There are two configuration files, which are intended for two different scenarios.**
> (for this guide, we will use `setenv.c`)
> - ~`input.local` → Advanced customisation of the final system.~
> - `setenv.c` → Simple customisation for the demo system. _**Use this file for the hands-on workshop!**_.
>
> In either case, the configuration file (or local input file) must be sourced in the existing shell (i.e. loaded into the current Shell’s environment).
>
> If you make an update to the configuration files, the `source` command must be run again.
> 
> Remember, `/vagrant` on the VM is shared with your local host system - with the git pull (or sparse checkout), the two files were pulled into the same directory as the `Vagrantfile`, which is accessible in the VM under `/vagrant`.

> [!VIDEO]
> Start of video [2.02B - Prepare SMS Host Parameters](https://www.youtube.com/watch?v=NnAUQqv4edU&list=PL2s6Yr_Iu_ke16_di1C3dowXHF-hRLmC9&index=5)

1) SSH to the SMS host VM and elevate to root

    ```bash
    [~/openhpc-handson/student/]$ vagrant ssh
    [vagrant@sms-host]$ sudo su
    ```

    > [!TIP]
    > **It is not best practice to run commands as root!**
    >
    > Since this is a test lab, it is considerably easier to issue commands as root and not have to worry about the occasional 'sudo' workarounds.

2) Examine the current environment variable status

    ```bash
    [root@sms-host vagrant]# echo $sms_name
    [root@sms-host vagrant]# echo $sms_ip
    ```
    
    Both commands will show _no response_, or rather, a _blank response_.

3) After sourcing the local input file, the environment variables respond with the values that were defined in the **local input** file (i.e. in `setenv.c` or ~`input.local`~):

    ```bash
    [root@sms-host vagrant]# source /vagrant/setenv.c
    [root@sms-host vagrant]# echo $sms_name 

    # OUTPUT: sms-host

    [root@sms-host vagrant]# echo $sms_ip 

    # OUTPUT: 10.10.10.10
    ```

4) Once the input file has been ‘sourced’, all the environment variables should be set for the current Shell session.

    > [!NOTICE]
    > Every new Shell instance must have the input file re-sourced.

5) Populate the `/etc/hosts` file

    ```bash
    [root@sms-host vagrant]# echo ${sms_ip} ${sms_name} ${sms_name}.hpcnet >> /etc/hosts
    ```

6) OpenHPC recommends disabling SELinux

    ```bash
    [root@sms-host vagrant]# perl -pi -e "s/SELINUX=\S+/SELINUX=disabled/" /etc/selinux/config
    ```

7) Reboot the VM

    ```bash
    [root@sms-host vagrant]# reboot
    ```

    > [!IMPORTANT]
    > When rebooting the VM from within the guest OS, the `/vagrant` shared directory might not remap on reboot. If the mapping is lost, a `vagrant reload` will reload the configuration file.
    >
    > Alternatively, a `vagrant halt` (graceful shutdown) followed by `vagrant up` will reboot the VM (also reloading the Vagrantfile), maintaining the shared directory mapping.

    > [!TIP]
    > **Always run as root and re-source the environment variables file on every new Shell.**
    >
    > Since this is a test lab, it is considerably easier to issue commands as root and not have to worry about the occasional ‘sudo’ workarounds, and OpenHPC makes extensive use of the variable parameters - if you do not source the local input file then these variables will hold blank values.

> [!VIDEO]
> Start of video [2.02C - Prepare SMS Host Parameters](https://www.youtube.com/watch?v=aXeKznNyAEQ&list=PL2s6Yr_Iu_ke16_di1C3dowXHF-hRLmC9&index=6)

8) (on reboot) restore to root profile for the Lab and re-source the environment

    ```bash
    [vagrant@sms-host ~]$ sudo su
    [root@sms-host vagrant]# source /vagrant/setenv.c
    ```

9) Disable the firewall

    ```bash
    [root@sms-host vagrant]# systemctl disable firewalld
    [root@sms-host vagrant]# systemctl stop firewalld
    ```

    > [!TIP]
    > **It is not best practice to run a public-facing server without a firewall!**
    >
    > Since this is a test lab, it is considerably easier to issue commands that will not be blocked by a firewall. The OpenHPC install guide follows this philosophy.

> [!CONGRATS]
> You have now installed and prepared the **SMS (System Management Server) Host** Virtual Machine for the OpenHPC components.
>
> The next step is to add the necessary OpenHPC packages in order to provision and manage the Virtual Cluster.

## 2.3. Enable OpenHPC components

> [!VIDEO]
> Start of video [2.03 - Enable OpenHPC Components](https://www.youtube.com/watch?v=kHorNuBgddY&list=PL2s6Yr_Iu_ke16_di1C3dowXHF-hRLmC9&index=7)

### 2.3.1. OpenHPC repository

You will need to enable the OpenHPC repository for local use - this requires external internet access from your master *SMS host* to the OpenHPC repository which is hosted on the internet. The download size is approx. **15kB**

```bash
[root@sms-host vagrant]# yum install http://build.openhpc.community/OpenHPC:/1.3/CentOS_7/x86_64/ohpc-release-1.3-1.el7.x86_64.rpm  
```

### 2.3.2. EPEL repository

As with [2.3.1](#_231-openhpc-repository) above, enable the EPEL repository for local use - this requires external internet access from your master *SMS host* to the internet.

```bash
[root@sms-host vagrant]# yum install -y epel-release
```

### 2.3.3. xCAT repository

As above, enable the xCAT and xCAT dependencies repositories for local use - this requires external internet access from your master *SMS host* to the internet.

```bash
[root@sms-host vagrant]# yum -y install yum-utils 
[root@sms-host vagrant]# wget -P /etc/yum.repos.d https://xcat.org/files/xcat/repos/yum/latest/xcat-core/xcat-core.repo --no-check-certificate
[root@sms-host vagrant]# wget -P /etc/yum.repos.d https://xcat.org/files/xcat/repos/yum/xcat-dep/rh7/x86_64/xcat-dep.repo --no-check-certificate
```

### 2.3.4. Provisioning services

In this step we perform 4 actions. We want to:
1) Add common base package and xCAT provisioning system to SMS (~240MB).
2) The OHPC base is approx. (~68MB).
3) Source the xCAT environment file into our current shell.
4) Configure a Network Time Protocal (NTP) service to run on the SMS host.

```bash
# Install base meta-package
[root@sms-host vagrant]# yum -y install ohpc-base
[root@sms-host vagrant]# yum -y install xCAT

# enable xCAT tools for use in current shell
[root@sms-host vagrant]# . /etc/profile.d/xcat.sh

# Configure the NTP service using ${ntp_server} environment variable
[root@sms-host vagrant]# systemctl enable ntpd.service
[root@sms-host vagrant]# echo "server ${ntp_server}" >> /etc/ntp.conf
[root@sms-host vagrant]# systemctl restart ntpd
```

> [!TIP]
> As with other sourced files, the `xcat.sh` file must be re-sourced whenever a new Shell process is invoked.

## 2.4. Resource Management: Slurm

> [!VIDEO]
> Start of video [2.04 - Resource Management Slurm](https://www.youtube.com/watch?v=MNfChQu5Gm8&list=PL2s6Yr_Iu_ke16_di1C3dowXHF-hRLmC9&index=8)

1) We need to provision a distributed resource management to SMS host. The HPC Ecosystems community has adopted Slurm workload manager. This is provided by the OpenHPC Slurm Server (~22MB).

    ```bash
    [root@sms-host vagrant]# yum -y install ohpc-slurm-server
    ```

2) The default configuration for Slurm will not be correct for this lab. Confirm the contents of the default `slurm.conf` file and then change it to match the environment for the lab.

    ```bash
    [root@sms-host vagrant]# cat /etc/slurm/slurm.conf | grep ControlMachine

    # OUTPUT: ControlMachine=linux0

    [root@sms-host vagrant]# perl -pi -e "s/ControlMachine=\S+/ControlMachine=${sms_name}/" /etc/slurm/slurm.conf
    [root@sms-host vagrant]# cat /etc/slurm/slurm.conf | grep ControlMachine

    # OUTPUT: ControlMachine=sms-host
    ```
    The above perl command replaces the default Slurm ControlMachine name (linux0) with the system defined name at `${sms_name}` (which is default to sms-host).

    > [!TIP]
    > A quick test of the very first change we made to `/etc/hosts` will verify that the SMS host can identify its own DNS entry.
    >
    > ```bash
    > [root@sms-host vagrant]# ping sms-host
    > ```
    >
    > This should return output that looks like the below:
    > ```text
    > 64 bytes from sms-host (127.0.0.1): icmp_seq=1 ttl=64 time=0.040 ms
    > 64 bytes from sms-host (127.0.0.1): icmp_seq=2 ttl=64 time=0.164 ms
    > ```

3) The default Slurm configuration file included with OpenHPC assumes dual-socket, 8 cores per socket, and 2 threads per core. However, **our Virtual Cluster** is designed with **single socket**, **2 cores per socket**, and **1 thread per core**. See Table below:

    |                   | OpenHPC | **HPC Ecosystems** |
    | ----------------- | :-----: | :------------: |
    | Socket Count      | 2       | 1              |
    | Cores per Socket  | 8       | 2              |
    | Threads per Core  | 2       | 1              |

    ```bash
    [root@sms-host vagrant]# cat /etc/slurm/slurm.conf | grep Sockets
    
    # OUTPUT:
    # NodeName=c[1-4] Sockets=2 CoresPerSocket=8 ThreadsPerCore=2 State=UNKNOWN
    ```

    > [!TIP] 
    > For this workshop there are various approaches to changing the `slurm.conf` file:
    > - Perl regular expressions _(demonstrated)_
    > - Slurm online configuration tool
    > - manual edits
    > - replacing `slurm.conf` with `slurm.ecos.conf` included in the Git repository

4) Using Perl to replace the parameters:

    ```bash
    [root@sms-host vagrant]# perl -pi -e "s/Sockets=2/Sockets=1/" /etc/slurm/slurm.conf
    [root@sms-host vagrant]# perl -pi -e "s/CoresPerSocket=8/CoresPerSocket=2/" /etc/slurm/slurm.conf
    [root@sms-host vagrant]# perl -pi -e "s/ThreadsPerCore=2/ThreadsPerCore=1/" /etc/slurm/slurm.conf
    ```
    
    Checking the contents of `slurm.conf` should now reveal that the compute nodes are defined as **c[1] to c[4] with 1 Socket, 2 Cores, 1 Thread**:

    ```bash
    [root@sms-host vagrant]# cat /etc/slurm/slurm.conf | grep Sockets

    # OUTPUT:
    # NodeName=c[1-4] Sockets=1 CoresPerSocket=2 ThreadsPerCore=1 State=UNKNOWN
    ```

5) Replace the compute node names to what is defined in `${c_name[0]}` and `${c_name[1]}` from the `setenv.c` configuration file, that is, replace `NodeName=c[1-4]` to `NodeName=compute0[0-1]`. This will specify a new NodeName prefix as well as a new node range of 2 nodes.

    ```bash
    [root@sms-host vagrant]# perl -pi -e "s/NodeName=c\[1-4\]/NodeName=compute0\[0-1\]/" /etc/slurm/slurm.conf
    [root@sms-host vagrant]# cat /etc/slurm/slurm.conf | grep Sockets

    # OUTPUT:
    # NodeName=compute0[0-1] Sockets=1 CoresPerSocket=2 ThreadsPerCore=1 State=UNKNOWN
    ```

    > [!TIP]
    > The perl command for replacing the `NodeName` uses special break characters (`\`) to accommodate for the character symbols `[` and `]`.

6) Update `PartitionName` parameter for node names to reflect the newly updated `compute0[0-1]` node names.

    ```bash
    [root@sms-host vagrant]# cat /etc/slurm/slurm.conf | grep Node

    # OUTPUT:
    # PartitionName=normal Nodes=c[1-4] Default=YES MaxTime=24:00:00 State=UP

    [root@sms-host vagrant]# perl -pi -e "s/Nodes=c\[1-4\]/Nodes=compute0\[0-1\]/" /etc/slurm/slurm.conf
    [root@sms-host vagrant]# cat /etc/slurm/slurm.conf | grep Node
    
    # OUTPUT:
    # NodeName=compute0[0-1] Sockets=1 CoresPerSocket=2 ThreadsPerCore=1 State=UNKNOWN
    # PartitionName=normal Nodes=compute0[0-1] Default=YES MaxTime=24:00:00 State=UP
    ```

## 2.5. Create a VM snapshot

> [!VIDEO]
> Start of video [2.05 - How to Snapshot a VM in the Virtual Lab](https://www.youtube.com/watch?v=GRjSJhlDbsU&list=PL2s6Yr_Iu_ke16_di1C3dowXHF-hRLmC9&index=9)

While it is plausible to run the entire Virtual Lab without making any backups of your progress, it is advisable to at least make snapshots at major milestones (such as at the end of primary chapter numbers - 2.xx, 3.xx, 4.xx and so on). Be aware that too many snapshots can bloat your resource usage and will increase the amount of disk storage you will need to host the VMs. 

The two recommended methods for taking a snapshot are:

1) **Through the VirtualBox Manager GUI (Figure 2)**

<p align="center">
 <img id="fig2" alt="Figure 2: How to snapshot a VM using the VirtualBox GUI" src="./_media/figure2.png" />
</p>
<center>Figure 2: How to snapshot a VM using the VirtualBox GUI</center>

2) **Through the commmand prompt:**

    ```bash
    [~/openhpc-handson/student/]$ vagrant snapshot save random-snapshot-name
    ```
