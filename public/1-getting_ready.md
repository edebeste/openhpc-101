# 1. Getting Ready: Virtual Lab Environment

## 1.0. Chapter Overview

This section will guide you through the setup of your Virtual Lab environment, using VirtualBox (although alternative Virtualisation Hypervisors _should_ have similar results).

The initial preparation focuses on the **System Management Server (SMS)** Host, which manages the Virtual Cluster. 

The Virtual Lab deployment uses a Vagrant definition file for the SMS Host to ensure straightforward and consistent SMS Host VM deployment. Ultimately the Virtual Cluster will consist of **three VM’s** (one host, two clients). 

> [!NOTICE]
> Anticipated time to complete this chapter: 20 minutes.

> [!IMPORTANT]
> This guide uses **VirtualBox** for the virtual lab environment.
>
> **Vagrant** is used to _manage_ the VirtualBox VM _configuration_.
>
> Although it is technically feasible to run a VM as part of a physically deployed HPC system,
> _YOU DO **NOT** NEED VIRTUALBOX OR VAGRANT FOR THE FINAL HPC DEPLOYMENT._
>
> Vagrant and VirtualBox are used for this Virtual Lab to simplify both the deployment process and to simulate a Virtual Cluster.

## 1.1. VM Setup (Overview)

The SMS Host VM will be pre-configured with the standard software environment used in the hands-on workshop and the official OpenHPC Installation Guide but the VM can also be replicated at home, in the office, in a lab, or in the bath. 

> [!NOTICE]
> Results may vary if you choose to run your Virtual Cluster in the bath.

The custom additions to the base VM image include:

- Additionally installed packages
    - Such as **`tmux`; `screen`; `vim`; `Git`**
- `setenv.c`, which replaces the older `input.local` file from the standard OpenHPC deployment

The SMS Host hardware configuration is specified by Vagrant and will be outlined in the Vagrant section below.

## 1.2 Vagrant overview, setup, and reset (Overview)

> [!VIDEO]
> Start of video [1.02 - Vagrant Overview, Setup, and Reset](https://www.youtube.com/watch?v=fqt4g-ucpAI)

The **Vagrantfile** parameters are based off the **[CentOS-7.7 bento](https://app.vagrantup.com/bento/boxes/centos-7.7)** image. The image is approx. 420MB in size.

The **System Management Server (SMS)** is defined as follows:

- 1GB RAM
- 2 virtual CPU’s with a total of 50% cap on host CPU usage
- Hostname **sms-host**
- Virtual Cluster NIC’s
- Additional parameters as defined by the **bento/centos-7.7** box

In addition, the location of the Vagrantfile is a **_shared directory_** with the SMS VM (between the host machine and the VM). 

> [!TIP]
> Refer to the [FAQ](7-faq#faq-installing-openhpc) on steps to reset a Vagrant image.

## 1.3. OpenHPC design (Overview)

The OpenHPC Guide relies on `${variable}` substitution in the command-line. Although it is possible to manually substitute these variables at every step, it is recommended to use the `setenv.c` file to define the IP addresses, hardware MAC addresses, and so on.

> [!TIP]
> The default values defined in `setenv.c` have been thoroughly tested for the Virtual Lab deployment as outlined in this guide and it is not recommended to modify them.
>
> _The file has a `.c` extension to allow syntax highlighting in compatible text editors (e.g. **vim**)._

## 1.4. Virtual Lab & Virtual Cluster (Overview)

This section provides a step-by-step guide for setting up the Virtual Lab environment. The Virtual Lab will configure **three lightweight virtual machines** that will be used to create a **Virtual Cluster** using the **VirtualBox** hypervisor. See **Figure 1**.

If resources do not allow for the creation and operation of the three VMs mentioned above, the process can be concluded with two VMs as a proof-of-concept.

> [!IMPORTANT]
> Make sure you understand the overview because you will be adding your specifications to the figure!

> [!NOTICE]
> Whereas the OpenHPC recipe expects four compute nodes, we will work with two.


[<img id="fig1" alt="Figure 1: Overview of the Virtual Cluster layout" src="./_media/figure1.png" />]()
<center>Figure 1: Overview of the Virtual Cluster layout</center>

## 1.5. Install Virtual Lab environment (Hands-On)

> [!VIDEO]
> Start of video [1.05 - Install Virtual Lab Environment](https://www.youtube.com/watch?v=DMyahaH2BuE&list=PL2s6Yr_Iu_kcX9ca6TLK_FHwad3_GbYWX&index=3)

### 1.5.1 Install VirtualBox

**VirtualBox** is used to host the Virtual Cluster. 

We will concentrate on deploying the **_Management Server (SMS host)_** as a VM (although this VM can also be used as the final Management Server solution for the physical HPC system, we do not recommend this as a long-term solution).

> [!NOTICE]
> This Virtual Management Server is **not** required for the final, physical HPC system.
>
> The Virtual SMS host is a sample replica for the final physical SMS host that will be deployed during a formal HPC system deployment; however, all the steps followed in the Virtual SMS host will be identical to those followed on the physical SMS host.
>
> The Virtual SMS host - with some minor tweaks - can in fact manage a physical HPC cluster.

- [Download VirtualBox](https://www.virtualbox.org/wiki/Downloads)
    - VirtualBox is a cross-platform type-2 hypervisor available as Open Source Software under GPL version&nbsp;2.
    - Supported host operating systems include Windows, Linux, Macintosh and Solaris.

    > [!IMPORTANT]
    > Individuals who are running the Virtual Lab on a Microsoft Windows machine you may have some trouble with the latest release of VirtualBox.
    >
    > It is recommended in this case that you use [VirtualBox Version 6.0.24](https://www.virtualbox.org/wiki/Download_Old_Builds_6_0), which has thus far proven stable in our test environment.

    > [!TIP]
    > Always be mindful to keep the precise version of the software as outlined in this guide. If no version is explicitly mentioned, then it is safe to assume that the most current version will work, although where possible, it is advised to use the same versions as were most current at the time of OpenHPC v1.3.x 

- Install Virtualbox

Once VirtualBox is installed, the framework for running VM’s is in place. Whereas one can manually create a VM inside the VirtualBox Manager (or via the command line) there is no guarantee of parity between _your_ created VM and the VM expected by the guide. To ensure configuration parity between the VM on your host machine and the one referenced in the guide, **Vagrant** will be implemented. 

### 1.5.2. Install Vagrant

Vagrant provides an automated mechanism to build and maintain VMs. It will configure the SMS VM to precise specifications for the guide and makes sure every user will have the same setup, every time.

> [!NOTICE]
> Vagrant is **not** required for the final HPC system.
> 
> Vagrant is a provisioning tool that ensures that all instances of a vagrant-managed Virtual Machine are identical upon each instantiation. This is essential for parity across all training workshops, but it is likely not particularly useful for a final physical HPC deployment, where systems will not be frequently redeployed.

- [Download and install Vagrant.](https://www.vagrantup.com/downloads.html)

    > [!TIP]
    > Be sure to select the package that matches your host machine’s operating system, not the CentOS VM! **The system may need to be rebooted**.

### 1.5.3 Set up VM / Vagrant parameters

The Vagrant parameters are stored in a **vagrantfile**. We provide a preconfigured `Vagrantfile` file (along with other critical files required for this lab).

There are two options for obtaining the testbed VM settings:

1) Use Git to clone the HPC Ecosystems OpenHPC repository (**recommended**).
2) Directly copy the Vagrantfile from the Git repository.

#### 1.5.3.1. Option 1: Clone Git repository with Git (recommended)

This option will keep any updates or changes synchronised with your system and is the best option for a hands-on workshop. 

> [!TIP]
> Updating files in the middle of an OpenHPC deployment that is using Vagrant can cause stability issues and is not recommended. Nonetheless, when carefully managed, a cloned Git repository is the most suitable approach during a class setting to allow swift updates to a multitude of participants.

1) Install **Git**. Depending on your openating system environment, you may have to choose one of the following options:
    - Git BASH for Windows https://gitforwindows.org/
    - Git for Linux
    - Git for MacOS

2) Navigate to the preferred **Git root** for this guide. For example `<your home directory>/openhpc-handson`.

    > [!NOTICE]
    > The **Git root** will be the base file directory location for the Virtual Cluster files, for example `/home/me/openhpc-handson` or `c:/users/me/Desktop/openhpc-handson`

    > [!NOTICE]
    > For the purpose of this guide the **Git root** will be `~/openhpc-handson/`
    > 
    > The sample syntax `[git root]#` can be interpreted as a prompt at `~/openhpc-handson/`
    >
    > The **smshost root** will be `~/openhpc-handson/smshost/`

3) **Initialise** the Git repository

    ```bash
    [git root]# git init
    ```

4) As a student, you will only require the `student/` directory in the Git repository, which can be sparsely checked out.

    > [!NOTICE]
    > The latest version of Git offers a much simpler method for sparse-checkout but the following instructions above should offer greater compatibility with older versions that might already be installed.

    ```bash
    [git root]# git init
    [git root]# git remote add origin https://github.com/brattex/hpc-ecosystems-openhpc101.git
    [git root]# git checkout -b 'student_branch'
    [git root]# git config core.sparsecheckout true
    [git root]# echo 'student' >> .git/info/sparse-checkout
    [git root]# git pull origin master
    ```

5) All the files stored in the remote Git repository that are necessary for the student experience are copied to a directory `student/` that is a subdirectory (child) of the directory you specified as the **Git root**.

    The most important files right now are:

    | File              | Description                                                                                                     |
    |-------------------|-----------------------------------------------------------------------------------------------------------------|
    | ~`input.local`~   | ~The official OpenHPC configuration parameter file in **shell** format for the OpenHPC auto-deployment script.~ |
    | `setenv.c`        | The hands-on configuration parameter file in **script** format for the Virtual Cluster Lab.                     |
    | `Vagrantfile`     | The Vagrant configuration file for the VMs.                                                                     |
    | `slurm.ecos.conf` | The customised SLURM configuration file for the Virtual Lab.                                                    |

#### 1.5.3.2. Option 2: Download the files from Git repo

This option will not use **Git** for the guide. This will not synchronise with the files on the OpenHPC Ecosystems repository. You will need to manually copy the files you need for the workshop to your PC. This is suitable for individuals running through this guide outside of a group or class but is not recommended.

1) Navigate to the directory where you want to install the Vagrant VM. This will be referred to as the head / root of the directory. \
    **For example:** `~/openhpc-handson/smshost/`

2) Download the HPC Ecosystems SMS-host `Vagrantfile` and `setenv.c` files into this location.
    - https://raw.githubusercontent.com/brattex/hpc-ecosystems-openhpc101/master/student/Vagrantfile (save as `Vagrantfile` with no file extensions) \
    (and `./setenv.c`)

    ```bash
    [~/openhpc-handson/smshost/]$ wget https://raw.githubusercontent.com/brattex/hpc-ecosystems-openhpc101/master/student/Vagrantfile
    [~/openhpc-handson/smshost/]$ wget https://raw.githubusercontent.com/brattex/hpc-ecosystems-openhpc101/master/student/setenv.c
    ```

    OR

    ```bash
    [~/openhpc-handson/smshost/]$ curl https://raw.githubusercontent.com/brattex/hpc-ecosystems-openhpc101/master/student/Vagrantfile > Vagrantfile
    [~/openhpc-handson/smshost/]$ curl https://raw.githubusercontent.com/brattex/hpc-ecosystems-openhpc101/master/student/setenv.c > setenv.c
    ```

> [!CONGRATS]
> The **Virtual Lab environment** is ready to deploy the Virtual Cluster!

> [!RECAP]
> In this Chapter the Virtual Lab environment was deployed:
>
> **Virtualbox** - this will host the virtual machines used in the Virtual Cluster.
>
> **Vagrant** - this will manage the virtual machine configurations.
>
> `Vagrantfile` - this file is copied to a target **smshost** VM directory and will define the parameters for the **smshost** VM.
