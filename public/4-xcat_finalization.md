# 4. xCAT Finalization

## 4.0. Chapter Overview

> [!NOTICE]
> **Anticipated time to complete this chapter: 30 minutes**

## 4.1. Finalizing provisioning configuration

> [!VIDEO]
> Start of video [4.01 - Finalizing provisioning configuration](https://www.youtube.com/watch?v=WpeU9MG-Nv0&list=PL2s6Yr_Iu_kfOmhsxBSstz00p10uOo9eY&index=1)

- Pack the stateless image from the chroot environment

    ```bash
    # Assemble final compute image using packimage 
    [root@sms-host vagrant]# packimage centos7.7-x86_64-netboot-compute

    # OUTPUT: 
    # Packing contents of /install/netboot/centos7.7/x86_64/compute/rootimg
    # archive method:cpio
    # compress method:gzip
    ```

## 4.2. Add compute nodes into xCAT database

> [!VIDEO]
> Start of video [4.02A -  Add Compute Nodes into xCAT Database](https://www.youtube.com/watch?v=IWt9yKQzubo&list=PL2s6Yr_Iu_kfOmhsxBSstz00p10uOo9eY&index=2)

- After packing the stateless image, compute nodes must be added and defined as objects in the xCAT database.

    ```bash
    # Define nodes as objects in xCAT database
    # Issue the same sequence of commands for node[0] and node[1] -- practice makes perfect :)
    [root@sms-host vagrant]# mkdef -t node compute00 groups=compute,all ip=${c_ip[0]} mac=${c_mac[0]} netboot=xnba arch=x86_64 bmc=${c_bmc[0]} bmcusername=${bmc_username} bmcpassword=${bmc_password} mgt=ipmi serialport=0 serialspeed=115200

    # OUTPUT: 1 object definitions have been created or modified.
    ```
    Define second node as object in xCAT database:
    ```bash
    [root@sms-host vagrant]# mkdef -t node compute01 groups=compute,all ip=${c_ip[1]} mac=${c_mac[1]} netboot=xnba arch=x86_64 bmc=${c_bmc[1]} bmcusername=${bmc_username} bmcpassword=${bmc_password} mgt=ipmi serialport=0 serialspeed=115200

    # OUTPUT: 1 object definitions have been created or modified.
    ```
    Verify that the nodes are defined correctly in the xCAT database:
    ```bash
    [root@sms-host vagrant]# lsdef

    # OUTPUT:
    # compute00 (node)
    # compute01 (node)
    ```
    > [!VIDEO]
    > Start of video [4.02B -  Add Compute Nodes into xCAT Database](https://www.youtube.com/watch?v=1_cNK1x7Hc4&list=PL2s6Yr_Iu_kfOmhsxBSstz00p10uOo9eY&index=3)

    ```bash
    # Define network domain name for system-wide name resolution
    [root@sms-host vagrant]# chdef -t site domain=${domain_name}  

    # OUTPUT: 1 object definitions have been created or modified.

    [root@sms-host vagrant]# chdef -t site master=${sms_ip}   

    # OUTPUT: 1 object definitions have been created or modified.

    [root@sms-host vagrant]# chdef -t site nameservers=${sms_ip}   

    # OUTPUT: 1 object definitions have been created or modified.
    ```
    Verify changes applied to xCAT are set correctly:
    ```bash
    [root@sms-host vagrant]# tabdump site | grep “master”  

    # OUTPUT: "master","10.10.10.10",,

    [root@sms-host vagrant]# tabdump site | grep “nameservers”  

    # OUTPUT: "nameservers","10.10.10.10",,
    ```
- Once the compute nodes and domain are specified, we must define the provisioning mode and image for the compute group and use xCAT to complete configuration for network services DNS and DHCP.

    ```bash
    # Complete network service configurations 
    [root@sms-host vagrant]# makehosts
    ```
    > [!IMPORTANT]
    > `makehosts` has been reported to break the format of `/etc/hosts` .
    >
    > see [Cluster Name Resolution -xCAT 2.16.3 document](https://xcat-docs.readthedocs.io/en/stable/advanced/domain_name_resolution/domain_name_resolution.html) for `/etc/hosts` section
    > 
    >(this appears to break because of the syntax in `setenv.c`)

    ```bash
    [root@sms-host vagrant]# makenetworks
    [root@sms-host vagrant]# makedhcp -n
    ```

    Running `makedhcp -n` should result in the following messages:
    ```text
    Renamed existing dhcp configuration file to  /etc/dhcp/dhcpd.conf.xcatbak
    The dhcp server must be restarted for OMAPI function to work
    Warning: [sms-host]: No dynamic range specified for 10.0.2.0. If hardware discovery is being used, a dynamic range is required.
    Warning: [sms-host]: No dynamic range specified for 10.10.10.0. If hardware discovery is being used, a dynamic range is required.
    ```

    > [!TIP]
    > As a sanity check (and to correct any errors), your `/etc/hosts` file should resemble this layout and details:
    >
    > 127.0.0.1       sms-host        sms-host
    >
    >127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
    >
    >::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
    >
    >10.10.10.10 sms-host    sms-host.hpcnet
    >
    >10.10.10.100 compute00  compute00.hpcnet
    >
    >10.10.10.101 compute01  compute01.hpcnet

    ```bash
    [root@sms-host vagrant]# makedns -n
    ```

    `makedns -n` should provide the following message:
    ```text
    Handling sms-host in /etc/hosts.
    Handling localhost in /etc/hosts.
    Handling compute00 in /etc/hosts.
    Handling localhost in /etc/hosts.
    Handling compute01 in /etc/hosts.
    Getting reverse zones, this may take several minutes for a large cluster.
    Completed getting reverse zones.
    Updating zones.
    Completed updating zones.
    Restarting named
    Restarting named complete
    Updating DNS records, this may take several minutes for a large cluster.
    Completed updating DNS records.
    DNS setup is completed
    ```

    Associate desired provisioning image for computes:
    ```bash
    [root@sms-host vagrant]# nodeset compute osimage=centos7.7-x86_64-netboot-compute 

    # OUTPUT: 
    # compute00: netboot centos7.7-x86_64-compute
    # compute01: netboot centos7.7-x86_64-compute
    ```

> [!CONGRATS]
> The **Virtual Lab environment** has successfully deployed the Virtual Cluster with one Management Server and two Compute nodes!

> [!RECAP]
> In this Chapter you have concluded the Virtual Lab environment deployment: in addition to the virtual SMS host, two Compute Nodes have been initialised with xCAT.

## 4.3. Ready the compute node VMs

> [!VIDEO]
> Start of video [4.03A -  Ready the compute node VMs](https://www.youtube.com/watch?v=ey7e1NiLcaE&list=PL2s6Yr_Iu_kfOmhsxBSstz00p10uOo9eY&index=4)



- It is time to import the compute node VMs into the Virtual Lab.
- The command-line interface uses `VBoxManage` to import the OVA image files; ***this is run from the host terminal and not the SMS Host***.

    ```bash
    [~/openhpc-handson/student/]$ vboxmanage import openhpc-demo-client00.ova  --options keepallmacs
    [~/openhpc-handson/student/]$ vboxmanage import openhpc-demo-client01.ova  --options keepallmacs
    ```

> [!VIDEO]
> Start of video [4.03B -  Boot Compute Nodes](https://www.youtube.com/watch?v=Z5mNpP9a5Qc&list=PL2s6Yr_Iu_kfOmhsxBSstz00p10uOo9eY&index=5)

- Power up the virtual Compute nodes using `vboxmanage`:
    ```bash
    [~/openhpc-handson/student/]$ vboxmanage startvm openhpc-demo-client00
    [~/openhpc-handson/student/]$ vboxmanage startvm openhpc-demo-client01
    ```

    > [!IMPORTANT]
    > The SMS-host Network adapters must be configured as: 
    > - Adapter 1 : NAT
    > - Adapter 2 : Internal Network `hpcnet`.
